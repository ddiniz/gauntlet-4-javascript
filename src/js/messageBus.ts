export class MessageBus {
    public channels: MessageChannel[] = [];
    public subscribe(channelName: string, eventName: string, callbackFunction: Function): void {
        let channel = this.channels.find(c => c.name === channelName)
        if (!channel) {
            channel = new MessageChannel(channelName)
            this.channels.push(channel);
        }
        channel.subscribe(eventName, callbackFunction);
    }
    public unsubscribe(channelName: string, eventName: string): void {
        let channel = this.channels.find(c => c.name === channelName)
        if (!channel) {
            console.warn(`No channel named ${channelName}`)
            return;
        }
        channel.unsubscribe(eventName);
    }
    public publish(channelName: string, eventName: string, data: any): void {
        let channel = this.channels.find(c => c.name === channelName);
        if (!channel) {
            console.log(`No channel named ${channelName}`);
        } else {
            channel.publish(eventName, data);
        }
    }
}

export class MessageChannel {
    public name: string;
    public subscribers: any = {};
    constructor(name: string) {
        this.name = name;
    }
    public subscribe(eventName: string, callbackFunction: Function): void {
        let subscriber = this.subscribers[eventName];
        if (!subscriber)
            this.subscribers[eventName] = []
        this.subscribers[eventName].push(callbackFunction);
    }
    public unsubscribe(eventName: string): void {
        let subscriber = this.subscribers[eventName];
        if (!subscriber) {
            console.warn(`No event named ${eventName}`)
            return;
        }
        delete (this.subscribers[eventName]);
    }
    public publish(eventName: string, data: any): void {
        let event = this.subscribers[eventName];
        if (!event) {
            console.log(`No event named ${eventName}`);
            return;
        }
        event.forEach((ev: Function) => ev(data));
    }

}
var messageBus = new MessageBus();
export default messageBus;