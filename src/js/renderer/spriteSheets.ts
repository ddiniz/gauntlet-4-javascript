export var INVALID_TILE = { x: 0, y: 0 };
export var FLOORTILE_SPRITESHEET = {
  FLOOR_TILE: { x: 0, y: 0 },
  FLOOR_TILE_SHADOW_00: { x: 0, y: 16 * 1 },
  FLOOR_TILE_SHADOW_01: { x: 0, y: 16 * 2 },
  FLOOR_TILE_SHADOW_02: { x: 0, y: 16 * 3 },
  FLOOR_TILE_SHADOW_03: { x: 0, y: 16 * 4 },
  FLOOR_TILE_SHADOW_04: { x: 0, y: 16 * 5 },
  FLOOR_TILE_SHADOW_05: { x: 0, y: 16 * 6 },
  FLOOR_TILE_SHADOW_06: { x: 0, y: 16 * 7 },
};
export var WALLTILE_SPRITESHEET = {
  WALL_TILE_00: { x: 0, y: 0 },
  WALL_TILE_01: { x: 16 * 1, y: 0 },
  WALL_TILE_02: { x: 16 * 2, y: 0 },
  WALL_TILE_03: { x: 16 * 3, y: 0 },
  WALL_TILE_04: { x: 16 * 4, y: 0 },
  WALL_TILE_05: { x: 16 * 5, y: 0 },
  WALL_TILE_06: { x: 16 * 6, y: 0 },
  WALL_TILE_07: { x: 16 * 7, y: 0 },
  WALL_TILE_08: { x: 16 * 8, y: 0 },
  WALL_TILE_09: { x: 16 * 9, y: 0 },
  WALL_TILE_10: { x: 16 * 10, y: 0 },
  WALL_TILE_11: { x: 16 * 11, y: 0 },
  WALL_TILE_12: { x: 16 * 12, y: 0 },
  WALL_TILE_13: { x: 16 * 13, y: 0 },
  WALL_TILE_14: { x: 16 * 14, y: 0 },
  WALL_TILE_15: { x: 16 * 15, y: 0 },
  WALL_TILE_16: { x: 16 * 16, y: 0 },
  WALL_TILE_17: { x: 0, y: 16 * 1 },
  WALL_TILE_18: { x: 16 * 1, y: 16 * 1 },
  WALL_TILE_19: { x: 16 * 2, y: 16 * 1 },
  WALL_TILE_20: { x: 16 * 3, y: 16 * 1 },
  WALL_TILE_21: { x: 16 * 4, y: 16 * 1 },
  WALL_TILE_22: { x: 16 * 5, y: 16 * 1 },
  WALL_TILE_23: { x: 16 * 6, y: 16 * 1 },
  WALL_TILE_24: { x: 16 * 7, y: 16 * 1 },
  WALL_TILE_25: { x: 16 * 8, y: 16 * 1 },
  WALL_TILE_26: { x: 16 * 9, y: 16 * 1 },
  WALL_TILE_27: { x: 16 * 10, y: 16 * 1 },
  WALL_TILE_28: { x: 16 * 11, y: 16 * 1 },
  WALL_TILE_29: { x: 16 * 12, y: 16 * 1 },
  WALL_TILE_30: { x: 16 * 13, y: 16 * 1 },
  WALL_TILE_31: { x: 16 * 14, y: 16 * 1 },
  WALL_TILE_32: { x: 16 * 15, y: 16 * 1 },
  WALL_TILE_33: { x: 16 * 16, y: 16 * 1 },
  WALL_TILE_34: { x: 0, y: 16 * 2 },
  WALL_TILE_35: { x: 16 * 1, y: 16 * 2 },
  WALL_TILE_36: { x: 16 * 2, y: 16 * 2 },
  WALL_TILE_37: { x: 16 * 3, y: 16 * 2 },
  WALL_TILE_38: { x: 16 * 4, y: 16 * 2 },
  WALL_TILE_39: { x: 16 * 5, y: 16 * 2 },
  WALL_TILE_40: { x: 16 * 6, y: 16 * 2 },
  WALL_TILE_41: { x: 16 * 7, y: 16 * 2 },
  WALL_TILE_42: { x: 16 * 8, y: 16 * 2 },
  WALL_TILE_43: { x: 16 * 9, y: 16 * 2 },
  WALL_TILE_44: { x: 16 * 10, y: 16 * 2 },
  WALL_TILE_45: { x: 16 * 11, y: 16 * 2 },
  WALL_TILE_46: { x: 16 * 12, y: 16 * 2 },
  WALL_TILE_47: { x: 16 * 13, y: 16 * 2 },
  WALL_TILE_48: { x: 16 * 14, y: 16 * 2 },
  WALL_TILE_49: { x: 16 * 15, y: 16 * 2 },
  WALL_TILE_50: { x: 16 * 16, y: 16 * 2 },
  WALL_TILE_51: { x: 0, y: 16 * 3 },
  WALL_TILE_52: { x: 16 * 1, y: 16 * 3 },
  WALL_TILE_53: { x: 16 * 2, y: 16 * 3 },
  WALL_TILE_54: { x: 16 * 3, y: 16 * 3 },
  WALL_TILE_55: { x: 16 * 4, y: 16 * 3 },
  WALL_TILE_56: { x: 16 * 5, y: 16 * 3 },
  WALL_TILE_57: { x: 16 * 6, y: 16 * 3 },
  WALL_TILE_58: { x: 16 * 7, y: 16 * 3 },
  WALL_TILE_59: { x: 16 * 8, y: 16 * 3 },
  WALL_TILE_60: { x: 16 * 9, y: 16 * 3 },
  WALL_TILE_61: { x: 16 * 10, y: 16 * 3 },
  WALL_TILE_62: { x: 16 * 11, y: 16 * 3 },
  WALL_TILE_63: { x: 16 * 12, y: 16 * 3 },
  WALL_TILE_64: { x: 16 * 13, y: 16 * 3 },
  WALL_TILE_65: { x: 16 * 14, y: 16 * 3 },
  WALL_TILE_66: { x: 16 * 15, y: 16 * 3 },
  WALL_TILE_67: { x: 16 * 16, y: 16 * 3 },
};

import invalid from "../../assets/images/invalid.png";
import wall1 from "../../assets/images/wall1.png";
import wall2 from "../../assets/images/wall2.png";
import wall3 from "../../assets/images/wall3.png";
import wall4 from "../../assets/images/wall4.png";
import wall5 from "../../assets/images/wall5.png";
import wall6 from "../../assets/images/wall6.png";
import floor1 from "../../assets/images/floor1.png";
import floor2 from "../../assets/images/floor2.png";
import floor3 from "../../assets/images/floor3.png";
import floor4 from "../../assets/images/floor4.png";
import floor5 from "../../assets/images/floor5.png";
import floor6 from "../../assets/images/floor6.png";

export class SpriteSheetSingleton {
  public sheets: {} = null;
  public wallSheets: SpriteSheet[] = null;
  public floorSheets: SpriteSheet[] = null;
  private loadedSprites: number = 0;
  private totalSprites: number = 0;
  public canDraw: boolean = false;

  public constructor() {
    this.loadSpriteSheets();
  }

  private loadSpriteSheets() {
    console.log("loaded sprites");
    this.sheets = {
      invalid: new SpriteSheet("invalid", invalid, this.addLoaded),
      wall1: new SpriteSheet("wall1", wall1, this.addLoaded),
      wall2: new SpriteSheet("wall2", wall2, this.addLoaded),
      wall3: new SpriteSheet("wall3", wall3, this.addLoaded),
      wall4: new SpriteSheet("wall4", wall4, this.addLoaded),
      wall5: new SpriteSheet("wall5", wall5, this.addLoaded),
      wall6: new SpriteSheet("wall6", wall6, this.addLoaded),
      floor1: new SpriteSheet("floor1", floor1, this.addLoaded),
      floor2: new SpriteSheet("floor2", floor2, this.addLoaded),
      floor3: new SpriteSheet("floor3", floor3, this.addLoaded),
      floor4: new SpriteSheet("floor4", floor4, this.addLoaded),
      floor5: new SpriteSheet("floor5", floor5, this.addLoaded),
      floor6: new SpriteSheet("floor6", floor6, this.addLoaded),
    };
    this.totalSprites = Object.keys(this.sheets).length;
    this.wallSheets = [
      (this.sheets as any)["wall1"],
      (this.sheets as any)["wall2"],
      (this.sheets as any)["wall3"],
      (this.sheets as any)["wall4"],
      (this.sheets as any)["wall5"],
      (this.sheets as any)["wall6"],
    ];

    this.floorSheets = [
      (this.sheets as any)["floor1"],
      (this.sheets as any)["floor2"],
      (this.sheets as any)["floor3"],
      (this.sheets as any)["floor4"],
      (this.sheets as any)["floor5"],
      (this.sheets as any)["floor6"],
    ];
  }

  private addLoaded = () => {
    this.loadedSprites++;
    if (this.loadedSprites >= this.totalSprites) {
      this.canDraw = true;
      console.log("can draw = true");
    }
  };
}

export class SpriteSheet {
  public name: string;
  public image: any;
  constructor(name: string, rawImage: any, loadSignalFunction: Function) {
    this.name = name;
    this.image = new Image();
    this.image.src = rawImage;
    this.image.onload = () => {
      loadSignalFunction();
    };
  }
}
var spriteSheet = new SpriteSheetSingleton();
export default spriteSheet;
