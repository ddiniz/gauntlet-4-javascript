import Widget from "../gui/widget";

export default class Renderer {
  public ctx: CanvasRenderingContext2D;

  constructor(ctx: CanvasRenderingContext2D) {
    this.ctx = ctx;
  }

  public clear(canvasWidth: number, canvasHeight: number): void {
    this.ctx.clearRect(0, 0, canvasWidth, canvasHeight);
  }
}
