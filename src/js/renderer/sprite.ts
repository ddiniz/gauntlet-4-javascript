import { Coord, CoordAndDimensions } from "../utils";
import spriteSheet from "./spriteSheets";

export default class Sprite {
  public coord: Coord;
  public scaledWidth: number;
  public scaledHeight: number;
  public sheetCoordDims: CoordAndDimensions;
  public sheetName: string;
  public scale: number = 1;

  constructor(
    coord: Coord,
    sheetCoordDims: CoordAndDimensions,
    sheetName: string,
    scale: number = 1
  ) {
    this.coord = coord;
    this.sheetCoordDims = sheetCoordDims;
    this.sheetName = sheetName;
    this.scale = scale;
    this.scaledWidth = sheetCoordDims.w * scale;
    this.scaledHeight = sheetCoordDims.h * scale;
  }

  public render(ctx: CanvasRenderingContext2D): void {
    ctx.drawImage(
      (spriteSheet.sheets as any)[this.sheetName].image,
      this.sheetCoordDims.x,
      this.sheetCoordDims.y,
      this.sheetCoordDims.w,
      this.sheetCoordDims.h,
      this.coord.x,
      this.coord.y,
      this.scaledWidth,
      this.scaledHeight
    );
  }
}
