import { Coord } from "../utils";
import { Key } from "./keys";

enum InputState {
  IDLE,
  FIRST_PRESS,
  HOLDING,
  RELEASED,
}

export class PlayerMouseInput {
  public tileCoord: Coord;
  public coord: Coord;
  public leftBtn: MouseInput;
  public rightBtn: MouseInput;
  public middleBtn: MouseInput;

  constructor() {
    this.coord = new Coord(0, 0);
    this.tileCoord = new Coord(0, 0);
    this.leftBtn = new MouseInput();
    this.rightBtn = new MouseInput();
    this.middleBtn = new MouseInput();
  }
}

export default class PlayerKeyboardInput {
  public up: KeyboardInput;
  public down: KeyboardInput;
  public left: KeyboardInput;
  public right: KeyboardInput;
  public ranged: KeyboardInput;
  public melee: KeyboardInput;
  public action1: KeyboardInput;
  public action2: KeyboardInput;

  constructor() {
    this.up = new KeyboardInput([Key.KeyW, Key.ArrowUp]);
    this.down = new KeyboardInput([Key.KeyS, Key.ArrowDown]);
    this.left = new KeyboardInput([Key.KeyA, Key.ArrowLeft]);
    this.right = new KeyboardInput([Key.KeyD, Key.ArrowRight]);
    this.ranged = new KeyboardInput([Key.KeyZ]);
    this.melee = new KeyboardInput([Key.KeyX]);
    this.action1 = new KeyboardInput([Key.Shift]);
    this.action2 = new KeyboardInput([Key.Space]);
  }
}

export class InputStateMachine {
  public keyDown: boolean = false;
  public state: InputState = InputState.IDLE;
  public isIdle(): boolean {
    return this.state === InputState.IDLE;
  }
  public isFirstPress(): boolean {
    return this.state === InputState.FIRST_PRESS;
  }
  public isHolding(): boolean {
    return this.state === InputState.HOLDING;
  }
  public isFirstPressOrHolding(): boolean {
    return (
      this.state === InputState.FIRST_PRESS || this.state === InputState.HOLDING
    );
  }
  public isReleased(): boolean {
    return this.state === InputState.RELEASED;
  }
  public runCurrentState(): void {
    switch (this.state) {
      case InputState.IDLE:
        if (this.keyDown) {
          this.changeState(InputState.FIRST_PRESS);
        }
        break;
      case InputState.FIRST_PRESS:
        if (this.keyDown) {
          this.changeState(InputState.HOLDING);
        } else {
          this.changeState(InputState.RELEASED);
        }
        break;
      case InputState.HOLDING:
        if (this.keyDown) {
          this.changeState(InputState.HOLDING);
        } else {
          this.changeState(InputState.RELEASED);
        }
        break;
      case InputState.RELEASED:
        if (this.keyDown) {
          this.changeState(InputState.FIRST_PRESS);
        } else {
          this.changeState(InputState.IDLE);
        }
        break;
    }
  }
  public changeState(newState: InputState): void {
    if (newState == this.state) return;
    this.state = newState;
  }
}

export class KeyboardInput extends InputStateMachine {
  public keys: string[] = [];

  constructor(keys: string[]) {
    super();
    this.keys = keys;
  }
}

export class MouseInput extends InputStateMachine {
  constructor() {
    super();
  }
}

function isKeyActive(event: KeyboardEvent, keyName: string): boolean {
  return event.key === keyName;
}

function atLeastOneKeyActive(
  event: KeyboardEvent,
  keyNames: string[]
): boolean {
  for (let keyName of keyNames) {
    if (isKeyActive(event, keyName)) {
      return true;
    }
  }
  return false;
}

export function checkAllKeyboardKeysStates(
  playerKeyboardInput: PlayerKeyboardInput,
  event: KeyboardEvent,
  keyDown: boolean
): void {
  for (let inputName in playerKeyboardInput) {
    let inp: KeyboardInput = (playerKeyboardInput as any)[inputName];
    if (atLeastOneKeyActive(event, inp.keys)) {
      inp.keyDown = keyDown;
    }
  }
}
