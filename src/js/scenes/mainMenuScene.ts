import { TextButton } from "../gui/button";
import Gui from "../gui/gui";
import { TextWidget } from "../gui/textWidget";
import spriteSheet from "../renderer/spriteSheets";
import Scene, { GameScenes } from "./scene";

export default class MainMenuScene extends Scene {
  private gui: Gui = null;

  constructor(canvas: HTMLCanvasElement, changeSceneCallBack: Function) {
    super(canvas, changeSceneCallBack);
    this.gui = new Gui();
    this.initializeGuiElements();
  }

  public handleInput(): void {
    this.gui.handleGuiInteraction();
  }
  public handleGameStates(): void {}
  public handlePhysics(): void {}
  public handleGraphics(ctx: CanvasRenderingContext2D): void {
    if (!spriteSheet.canDraw) return;
    this.gui.render(ctx);
  }

  private initializeGuiElements(): void {
    this.gui.widgets.push(
      new TextWidget(
        "Manopla",
        "22px Arial",
        this.canvas.width / 2,
        this.canvas.height / 2 - 200
      )
    );
    this.gui.widgets.push(
      new TextButton(
        "Map Editor",
        "15px Arial",
        this.canvas.width / 2,
        this.canvas.height / 2,
        90,
        32,
        () => {
          this.changeSceneCallBack(GameScenes.MAP_EDITOR);
        }
      )
    );
    this.gui.widgets.push(
      new TextButton(
        "Battle Mode",
        "15px Arial",
        this.canvas.width / 2,
        this.canvas.height / 2 + 100,
        90,
        32,
        () => {
          this.changeSceneCallBack(GameScenes.BATTLE);
        }
      )
    );
  }
}
