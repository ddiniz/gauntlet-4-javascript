import Renderer from "../renderer/renderer";

export enum GameScenes {
  MAIN_MENU,
  MAP_EDITOR,
  BATTLE,
}

export default abstract class Scene {
  canvas: HTMLCanvasElement = null;
  changeSceneCallBack: Function;

  constructor(canvas: HTMLCanvasElement, changeSceneCallBack: Function) {
    this.canvas = canvas;
    this.changeSceneCallBack = changeSceneCallBack;
  }
  public abstract handleInput(): void;
  public abstract handleGameStates(): void;
  public abstract handlePhysics(): void;
  public abstract handleGraphics(ctx: CanvasRenderingContext2D): void;
}
