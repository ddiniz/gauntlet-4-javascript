import mapJson from "../../assets/maps/testmap.json";
import { ImageButton, TextButton } from "../gui/button";
import Cursor from "../gui/cursor";
import Gui from "../gui/gui";
import input from "../input/InputSingleton";
import GameMap from "../map/gameMap";
import Sprite from "../renderer/sprite";
import spriteSheet from "../renderer/spriteSheets";
import { Coord, CoordAndDimensions } from "../utils";
import Scene from "./scene";

export default class MapEditorScene extends Scene {
  private gui: Gui = null;
  private cursor: Cursor = null;
  private gameMap: GameMap = null;

  private testMapNum = 0;
  private tileTypeMouseLeft = -1;
  private tileTypeMouseRight = 1;

  constructor(canvas: HTMLCanvasElement, changeSceneCallBack: Function) {
    super(canvas, changeSceneCallBack);
    this.gameMap = this.gameMap = new GameMap(mapJson);
    this.gui = new Gui();
    this.cursor = new Cursor(this.gameMap.mapWidth, this.gameMap.mapHeigth, 32);
    this.initializeGuiElements();
  }

  private generateTestTileMap(): void {
    this.gameMap.generateTestTileMap(this.testMapNum);
    console.log(this.testMapNum);
    if (this.testMapNum === 255) this.testMapNum = 0;
    else this.testMapNum++;
  }

  private importTileMap(): void {
    this.gameMap.exportMap(document, this.gameMap.simpleMap);
  }
  private exportTileMap(): void {
    this.gameMap.importMapStart(document);
  }

  private changeTileTypeMouseLeft(tileType: number): void {
    this.tileTypeMouseLeft = -tileType;
  }
  private changeTileTypeMouseRight(tileType: number): void {
    this.tileTypeMouseRight = tileType;
  }

  private changeMapTileToWall(mouseCoord: Coord): void {
    this.gameMap.changeMapTile(mouseCoord, this.tileTypeMouseLeft);
  }

  private changeMapTileToFloor(mouseCoord: Coord): void {
    this.gameMap.changeMapTile(mouseCoord, this.tileTypeMouseRight);
  }

  public handleInput(): void {
    if (input.keyboard.action1.isReleased()) {
      this.generateTestTileMap();
    }
    let interacted = this.gui.handleGuiInteraction();
    if (!interacted) {
      if (input.mouse.leftBtn.isFirstPressOrHolding()) {
        this.changeMapTileToWall(input.mouse.tileCoord);
      } else if (input.mouse.rightBtn.isFirstPressOrHolding()) {
        this.changeMapTileToFloor(input.mouse.tileCoord);
      }
    }
  }

  public handleGameStates(): void {}
  public handlePhysics(): void {}

  public handleGraphics(ctx: CanvasRenderingContext2D): void {
    if (!spriteSheet.canDraw) return;
    this.gameMap.render(ctx);
    this.cursor.render(ctx);
    this.gui.render(ctx);
  }

  private initializeGuiElements(): void {
    this.gui.widgets.push(
      new TextButton(
        "Import map",
        "22px Arial",
        this.canvas.width - 100,
        100,
        90,
        32,
        this.importTileMap
      )
    );
    this.gui.widgets.push(
      new TextButton(
        "Export map",
        "22px Arial",
        this.canvas.width - 100,
        150,
        90,
        32,
        this.exportTileMap
      )
    );
    let floorSprites = [
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor1",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor2",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor3",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor4",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor5",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(0, 0, 16, 16),
        "floor6",
        2
      ),
    ];
    let wallSprites = [
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall1",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall2",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall3",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall4",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall5",
        2
      ),
      new Sprite(
        new Coord(0, 0),
        new CoordAndDimensions(16 * 14, 16, 16, 16),
        "wall6",
        2
      ),
    ];
    for (let i = 0; i < floorSprites.length; i++) {
      let x = this.canvas.width - 135;
      let y = 200 + i * 38;
      let sprite = floorSprites[i];
      sprite.coord = new Coord(x, y);
      this.gui.widgets.push(
        new ImageButton(sprite, x, y, 32, 32, () =>
          this.changeTileTypeMouseRight(i + 1)
        )
      );
    }
    for (let i = 0; i < wallSprites.length; i++) {
      let x = this.canvas.width - 95;
      let y = 200 + i * 38;
      let sprite = wallSprites[i];
      sprite.coord = new Coord(x, y);
      this.gui.widgets.push(
        new ImageButton(sprite, x, y, 32, 32, () =>
          this.changeTileTypeMouseLeft(i + 1)
        )
      );
    }
  }
}
