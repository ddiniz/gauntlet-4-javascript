import Sprite from "../renderer/sprite";

export default class Tile {
  public tileType: TileType = TileType.FLOOR;
  public sprite: Sprite = null;
  public tileSpriteNumber: number;

  constructor(tileType: TileType) {
    this.tileType = tileType;
  }
}
export enum TileType {
  WALL,
  DESTRUCTIBLE,
  FLOOR,
}
