import input from "../input/InputSingleton";

export default class Cursor {
  public mapWidth: number;
  public mapHeigth: number;
  public scaledSpriteSize: number;

  constructor(mapWidth: number, mapHeigth: number, scaledSpriteSize: number) {
    this.mapWidth = mapWidth;
    this.mapHeigth = mapHeigth;
    this.scaledSpriteSize = scaledSpriteSize;
  }
  public render(ctx: CanvasRenderingContext2D): void {
    let mouse = input.getMouseTileCoord();
    let realPosY = mouse.y * this.scaledSpriteSize;
    let realPosX = mouse.x * this.scaledSpriteSize;
    if (
      realPosX < this.mapWidth * this.scaledSpriteSize &&
      realPosY < this.mapHeigth * this.scaledSpriteSize
    )
      ctx.strokeRect(
        realPosY,
        realPosX,
        this.scaledSpriteSize,
        this.scaledSpriteSize
      );
  }
}
