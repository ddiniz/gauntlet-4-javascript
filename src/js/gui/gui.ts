import input from "../input/InputSingleton";
import { Coord } from "../utils";
import { Button } from "./button";
import Widget from "./widget";

export default class Gui {
  public widgets: Widget[] = [];

  constructor() {}

  public handleGuiInteraction(): boolean {
    let mouseCoord: Coord = input.getMouseCoord();
    if (input.mouse.leftBtn.keyDown) {
      for (let widget of this.widgets) {
        if (widget instanceof Button && widget.tryActionIfPressed(mouseCoord))
          return true;
      }
    }
    return false;
  }

  public render(ctx: CanvasRenderingContext2D): void {
    for (let widget of this.widgets) {
      widget.render(ctx);
    }
  }
}
