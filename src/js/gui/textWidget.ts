import { CoordAndDimensions } from "../utils";
import Widget from "./widget";

export class TextWidget extends Widget {

  public text: string;
  public font: string;

  constructor(text: string, font: string, x: number, y: number) {
    super();
    this.coord = new CoordAndDimensions(x, y, 0, 0);
    this.text = text;
    this.font = font;
  }

  public render(ctx: CanvasRenderingContext2D): void {
    ctx.font = this.font;
    ctx.textAlign = "center";
    ctx.fillText(this.text, this.coord.x, this.coord.y);
  }
}
